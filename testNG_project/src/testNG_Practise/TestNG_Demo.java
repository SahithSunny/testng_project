package testNG_Practise;

import org.testng.annotations.Test;

public class TestNG_Demo {

	@Test
	public void test()
	{
		System.out.println("First TestNG Demo");
	}
	
	
	@Test
	public void another_test()
	{
		System.out.println("Here is the second Test");
	}
}
